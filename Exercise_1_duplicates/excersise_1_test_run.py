from excersise_1_run import run
from datetime import datetime
now = datetime.now()
time = now.strftime("%H:%M:%S")
a_file = open("test_case.txt", "r")
list_of_lists = [(line.strip()).split() for line in a_file]
a_file.close()
num_lines = sum(1 for line in open('test_case.txt'))
output = open("output.txt",'a')
with open('test_case.txt', 'r') as the_file:
    for i in range(num_lines):
        temp = list_of_lists[i].index('=>')
        ans = run(list_of_lists[i][:temp])
        line = the_file.readline()
        if 'False' in line and ans == 'false':
            print('correct')
            output.write('correct '+time+'\n')
        elif 'True' in line and ans == 'true':
            print('correct')
            output.write('correct '+time+'\n')
        else:
            print('wrong\n')
            output.write('correct '+time+'\n')