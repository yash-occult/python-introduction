# Excercise 1 #

Problem Statement:
Write a Python function that takes a sequence of numbers as list and determines whether all the numbers are different from each other.

### Logic? ###

* Fetch inputs from file
* Convert String to List
* Remove all after '=>' index
* import the run function from excersise_1_run.py file
* pass-in the list of numbers and convert list to set
* If duplicate found set will discard them
* Comparing the length of list and set 
* If length of list and set not equal then it contains duplicate
* Print the ouput to output.txt file

### How to run file? ###

* call the python file 'excersise_1_test_run.py' file
* check its output in output.txt file
* test